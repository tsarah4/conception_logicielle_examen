from serveur.core import getAllAlbums, getIdArtist, getAllSongs, getLyrics
import pytest


def test_getIdArtist():
    id = "150363"
    getIdArtist("eddy de pretto")==id

def test_getIdArtistbis():
    getIdArtist("eddy de prett")==None

def test_getAllAlbums():
    album=["2299166"]
    getAllAlbums("eddy de pretto")==album

def test_getAllSongs():
    songs=["Début", "Random", "Rue de Moscou", "Jimmy", "Beaulieue", "Quartier des lunes", "Desmurs",
            "Kid", "Normal", "Honey", "Genre", "Ego", "Mamere", "Fête de trop", "Musique Basse"]
    getAllSongs("eddy de pretto")==songs

def test_getLyrics():
    lyrics=	"Fais pas semblant\r\nJe ne quitte jamais mon masque\r\nAvec ce petit masque d'audience\r\nFait semblant\r\nFera genre parmi les riches"
    getLyrics("eddy de pretto", "Début")==lyrics