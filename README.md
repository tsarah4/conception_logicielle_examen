Examen de conception logicielle 2022

Cette application vous propose de manière aléatoire une chanson pour un artiste donné en entrée.

## Architecture de l'application :

```mermaid
graph TD;
  Client -- HTTP --> Serveur;
  Serveur --> Client;
  Serveur -- HTTP --> AudioDB;
  Serveur -- HTTP --> Lyricsovh;
  AudioDB -- json --> Serveur ;
  Lyricsovh -- json --> Serveur ;
```

## Installation de l'application : 

La première étape consiste à installer l'application. Pour cela, voici les commandes à suivre :
```
git clone https://gitlab.com/tsarah4/conception_logicielle_examen.git
cd conception_logicielle_examen
```

# Pour lancer le serveur :

La seconde étape consiste à lancer le serveur. Pour cela, voici les commandes à suivre dans un terminal :
```
cd serveur
pip install -r requirements.txt
uvicorn main:app --reload
```
Pour vérifier l'état de fonctionnement du serveur, copiez cette adresse dans une barre de recherche internet :
```
http://127.0.0.1:8000
```

Pour obtenir de manière aléatoire une chanson pour un artiste donné, copiez cette adresse dans une barre de recherche internet :
```
http://127.0.0.1:8000/random/{artist_name}
```

# Pour lancer le client :

Pour lancer le client, il faut d'abord lancer le serveur en suivant les étapes évoquées précédemment. Ensuite, voici les commandes à suivre dans un autre terminal :
```
cd conception_logicielle_examen
cd client
pip install -r requirements.txt
python main.py
```

# Pour lancer les tests :

Pour lancer les tests unitaires, il faut se placer à la racine du projet. Voici les commandes à suivre dans un autre terminal :
```
pip install -r requirements.txt
python -m pytest test/test.py
```
