import requests, random
from dotenv import load_dotenv
import os

load_dotenv() 

from fastapi import FastAPI

from core import *

app = FastAPI()
    
@app.get("/")
def healthcheck(): 
    if requests.get(os.environ.get("url_artist_check")).status_code == 200 and requests.get(os.environ.get("url_lyric_check")).status_code == 200 :
        return 200 
    elif requests.get(os.environ.get("url_artist_check")).status_code == 404 or requests.get(os.environ.get("url_lyric_check")).status_code == 404 :
        return 404

@app.get("/random/{artist_name}")
def read_artist_random(artist_name):
    songs=getAllSongs(artist_name)
    try :
        title = random.choice(songs)
        indice=songs.index(title)
        url_youtube = getAllUrlYoutube(artist_name)[indice]
    except : 
        title = None
        url_youtube = None
    lyrics = getLyrics(artist_name, title)
    return {"artist": artist_name, 
            "title" : title,
            "suggested_youtube_url" :  url_youtube,
            "lyrics" : lyrics
            }

