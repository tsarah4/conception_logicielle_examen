from dotenv import load_dotenv
import requests
import os

load_dotenv() 

def getIdArtist(artist_name : str):
    '''
    returns the Id of the artist using the API AudioDB

    parameter :
        artist_name : str
            the name of the artist

    return :
        idArtist : int or None if the artist was not found
            Id artist
    '''
    try :
        return requests.get(os.environ.get("api_artist").format(artist_name)).json()["artists"][0]["idArtist"]
    except :
        None


def getAllAlbums(artist_name : str):
    """
    returns all the Id of albums of the artist using the API AudioDB

    parameter : 
            artist_name : str
                the name of the artist

    return :
            idAlbums : list of int or None if no album was found
                list of Id of all the artist's albums
    """
    idArtist=getIdArtist(artist_name) 
    try:
        requete=requests.get(os.environ.get("api_album").format(idArtist))
        idAlbums = []
        for i in range(len(requete.json()['album'])):
            idAlbums.append(requete.json()['album'][i]['idAlbum'])
        return idAlbums
    except:
        return None


def getAllSongs(artist_name : str):
    """
    returns all the songs of the artist using the API audioDB

    parameter : 
            artist_name : str
                the name of the artist

    return :
            songs : list of str or None if no song was found
                list of all the songs of the artist
    """
    idAlbums=getAllAlbums(artist_name)
    try :
        songs=[]
        for album in idAlbums:
            requete=requests.get(os.environ.get("api_song").format(album))
            for i in range(len(requete.json()["track"])):
                songs.append(requete.json()['track'][i]['strTrack'])
        return songs
    except :
        return None

def getAllUrlYoutube(artist_name : str):
    """
    returns all the youtube url of the song of the artist using the API audioDB

    parameter : 
            artist_name : str
                the name of the artist

    return :
            urls : list of str or None if 
                list of all the youtube url of the songs of the artist
                a string can be null if the url does not exist
    """
    try :
        idAlbums=getAllAlbums(artist_name)
        urls=[]
        for album in idAlbums:
            requete=requests.get(os.environ.get("api_song").format(album))
            for i in range(len(requete.json()["track"])):
                    urls.append(requete.json()['track'][i]['strMusicVid'])
        return urls
    except :
        None


def getLyrics(artist_name : str, title : str):
    """
    returns the lyric of the song of the artist using the API Lyricsovh

    parameters : 
            artiste_name : str
                the name of the artist
            title : str
                the title of the song

    return :
            lyrics : str or None if no lyrics was found
    """
    try :
        return requests.get(os.environ.get("api_lyric").format(artist_name,title)).json()["lyrics"]
    except :
        None
