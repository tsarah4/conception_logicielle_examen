fastapi==0.75.2
requests==2.27.1
uvicorn==0.17.5
virtualenv==20.4.7
python-dotenv==0.20.0
