from dotenv import load_dotenv
import requests, json, random, os

load_dotenv()


def loadJson(file_json):
    """
    allows to load a json file

    parameter :
            file_json: str 
                the name of the json file

    returns : 
            file : dict 
    """
    with open(file_json) as file:
        return json.load(file)

def create_playlist(file_json):
    """
    create a playlist of 20 songs from json file

    parameter : 
            file_name : str 
                the name of the json file

    return : 
            playlist : list of dict with 4 keys (artiste_name, title, suggested_youtube_url, lyrics)
    """
    rudy=loadJson(file_json)
    artists= list(map(lambda x:x["artiste"], rudy))
    weights= list(map(lambda x:x["note"], rudy))
    playlist = []
    base_format = "n°{} : {} by {} \n (Suggested youtube URL:{}) \n Lyrics : \n {} \n "
    while len(playlist)<20: 
        artiste = random.choices(artists, weights=weights)[0]
        song = requests.get(os.environ.get("server_url")+"/random/"+artiste).json()
        if song["lyrics"] != None and song not in playlist: 
            playlist.append(song)
            print(base_format.format(len(playlist),song["title"],song["artist"],song["suggested_youtube_url"],song["lyrics"]))


if __name__ == "__main__":
    create_playlist('rudy.json')

